//
//  Person+CoreDataProperties.m
//  AddressMap
//
//  Created by Zachary Cole on 3/17/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Person+CoreDataProperties.h"

@implementation Person (CoreDataProperties)

@dynamic name;
@dynamic address;

@end
