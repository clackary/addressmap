//
//  AddViewController.h
//  AddressMap
//
//  Created by Zachary Cole on 3/17/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface AddViewController : UIViewController
- (IBAction)saveBtnTouched:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *namettxt;
@property (weak, nonatomic) IBOutlet UITextField *addresstxt;

@end
