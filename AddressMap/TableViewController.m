//
//  TableViewController.m
//  AddressMap
//
//  Created by Zachary Cole on 3/17/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import "TableViewController.h"

@interface TableViewController ()

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSError* error;
    NSFetchRequest* req = [[NSFetchRequest alloc] init];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:ad.managedObjectContext];
    [req setEntity:entity];
    _fetchedPersons = [ad.managedObjectContext executeFetchRequest:req error:&error];
    
    
    [self.tableView reloadData];
//    NSLog(@"Here.");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return self.fetchedPersons.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myID"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"myID"];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    //set name text
    cell.textLabel.text = [[_fetchedPersons objectAtIndex:indexPath.row] valueForKey:@"name"];
    //set address subtext
    cell.detailTextLabel.text = [[_fetchedPersons objectAtIndex:indexPath.row] valueForKey:@"address"];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Person* obj = (Person*)self.fetchedPersons[indexPath.row];
    [self performSegueWithIdentifier:@"toLocation" sender:obj];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"toLocation"]) {
        MapViewController* mvc = (MapViewController*)segue.destinationViewController;
        mvc.person = sender;
    }
    
//    if ([segue.identifier isEqualToString:@"toAdd"]) {
//        AddViewController* avc = (AddViewController*)segue.destinationViewController;
//        
//    }
}

//- (IBAction)addTouched:(id)sender {
//    [self performSegueWithIdentifier:@"toAdd" sender:nil];
//}

@end
