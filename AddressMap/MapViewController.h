//
//  MapViewController.h
//  AddressMap
//
//  Created by Zachary Cole on 3/17/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MapViewController : UIViewController<CLLocationManagerDelegate, MKMapViewDelegate>
{
    CLLocationManager* locationManager;
}

@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@property (nonatomic, retain) Person* person;

@property CLGeocoder* geocoder;

@end
