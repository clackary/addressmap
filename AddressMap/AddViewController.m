//
//  AddViewController.m
//  AddressMap
//
//  Created by Zachary Cole on 3/17/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import "AddViewController.h"

@interface AddViewController ()

@end

@implementation AddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveBtnTouched:(id)sender {
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSManagedObject* personEntity = [NSEntityDescription insertNewObjectForEntityForName:@"Person" inManagedObjectContext:ad.managedObjectContext];
    
    //Testing Core Data entry
    [personEntity setValue:self.namettxt.text forKey:@"name"];
    [personEntity setValue:self.addresstxt.text forKey:@"address"];
    NSError* error;
    if (![ad.managedObjectContext save:&error])
    {
        NSLog(@"Save failed.");
    }
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
