//
//  TableViewController.h
//  AddressMap
//
//  Created by Zachary Cole on 3/17/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import <UIKit/UIKit.h>
 #import "AppDelegate.h"
#import "MapViewController.h"
#import "AddViewController.h"
#import "Person.h"

@interface TableViewController : UITableViewController

@property(strong, nonatomic) NSArray* fetchedPersons;

@end
